version 0.2.1
* fix issue with disabling the Packer fix
* precache sprites from zips (removing config for skipping which files get left open)

version 0.2.0
* add option to skip leaving files open when reading assets for specific mods. Expand the Gungeon included by default bc of issues with how it opens zips.
* allow using `dump sprites` and other commands to override hooks disabling unity object hooks.

version 0.1.1
* fix error message appearing at start when using config file
* fix some loading errors

version 0.1.0
* Initial release
* Fixes for RuntimeAtlasPacker.Pack, AssetMetadata.GetStream, and UnityEngine Object hooks
* Disable UnityEngine Object hooks by default.
* Read configuration from Resources/gungeongobrrr.json