﻿using System;
using System.IO;

namespace GoBrrrMod
{
    internal static class LogHelper
    {
        public static readonly string LogPath = Path.Combine(Path.GetFullPath(ETGMod.ResourcesDirectory), "ggb_log.txt");

        private static StreamWriter Writer;

        public static void Log(string message)
        {
            InitializeWriter();
            string ts = GetTimestamp();
            Writer.WriteLine($"[{ts}] {message}");
            Writer.Flush();
        }

        public static void Log(Exception ex, string message)
        {
            InitializeWriter();
            string ts = GetTimestamp();
            Writer.WriteLine($"[{ts}] {message} {ex}");
            Writer.Flush();
        }

        private static string GetTimestamp()
        {
            return DateTime.Now.ToString("HH:mm:ss.fff");
        }

        private static void InitializeWriter()
        {
            if (Writer != null)
                return;

            Writer = new StreamWriter(LogPath, false);
        }
    }
}
