﻿using System.Collections.Generic;
using UnityEngine;

namespace GoBrrrMod
{
    public class EnhancedAtlasManager
    {
        public EnhancedAtlasManager(RuntimeAtlasPacker packer)
        {
            Packer = packer;
            AllManagedPages = new List<PageManager>();
            RecentPages = new LinkedList<PageManager>();
        }

        public RuntimeAtlasPacker Packer;

        public int MaxCacheSize = 1;

        /// <summary>
        /// These are the pages being managed by this page manager. Not all pages in the packer.
        /// </summary>
        public List<PageManager> AllManagedPages;

        public LinkedList<PageManager> RecentPages;

        public void Initialize()
        {
            //StatisticsManager.Instance.AtlasManagerInitalizeStopwatch.Start();

            foreach (var page in Packer.Pages)
            {
                var ex = new PageManager(page);
                ex.RealignForExistingPage();
                AllManagedPages.Add(ex);
                RecentPages.AddLast(ex);
            }

            while (RecentPages.Count > MaxCacheSize)
            {
                RecentPages.RemoveFirst();
            }

            //StatisticsManager.Instance.AtlasManagerInitalizeStopwatch.Stop();
        }

        public RuntimeAtlasSegment Pack(Texture2D tex, bool apply)
        {
            PageManager selectedPage = null;
            RuntimeAtlasSegment segment = null;
            foreach (var page in RecentPages)
            {
                if (page.TryPlaceOnPage(tex, out segment))
                {
                    selectedPage = page;
                    break;
                }
            }

            if (selectedPage == null)
            {
                var page = NewPage();
                if (!page.TryPlaceOnPage(tex, out segment))
                {
                    // something went really bad.
                    // disable ourselves so that we don't screw stuff up.
                    EtgModLogHelper.LogWarning($"Could not place texture '{tex.name}' on new RuntimeAtlasPage. Falling back to older RuntimeAtlasPacker.Pack.");
                    PackerManager.Instance.IsEnabled = false;
                    return Packer.Pack(tex, apply);
                }
            }

            if (apply)
            {
                selectedPage.Page.Apply();
            }

            return segment;
        }

        private PageManager NewPage()
        {
            var page = Packer.NewPage();
            var ex = new PageManager(page);
            AllManagedPages.Add(ex);
            RecentPages.AddLast(ex);
            while (RecentPages.Count > MaxCacheSize)
            {
                RecentPages.RemoveFirst();
            }

            return ex;
        }
    }
}
