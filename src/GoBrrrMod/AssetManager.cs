﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using GoBrrrMod.Configuration;
using Ionic.Zip;
using MonoMod.RuntimeDetour;
using UnityEngine;
using UnityObject = UnityEngine.Object;

namespace GoBrrrMod
{
    public class AssetManager
    {
        private static readonly StatisticsManager StatsManager = StatisticsManager.Instance;

        private Hook _assetMetadataGetStreamHook;
        private Hook _assetMetadataGetDataHook;
        private Hook _assetsLoadHook;
        private Hook _assetsReplaceTextureHook;

        private Dictionary<string, Dictionary<string, byte[]>> _cachedZipFiles = new Dictionary<string, Dictionary<string, byte[]>>();

        private AssetManager()
        {
        }

        public static AssetManager Instance { get; } = new AssetManager();

        public bool IsEnabled;

        public void Initialize(AssetConfiguration config)
        {
            StatsManager.AssetManagerStartupStopwatch.Start();
            StatsManager.AssetManagerBytesCached = 0L;

            try
            {
                const BindingFlags Flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
                _assetMetadataGetStreamHook = new Hook(
                    typeof(AssetMetadata).GetProperty(nameof(AssetMetadata.Stream)).GetGetMethod(),
                    typeof(AssetManager).GetMethod(nameof(OnAssetMetadataGetStream), Flags));

                _assetMetadataGetDataHook = new Hook(
                    typeof(AssetMetadata).GetProperty(nameof(AssetMetadata.Data)).GetGetMethod(),
                    typeof(AssetManager).GetMethod(nameof(OnAssetMetadataGetData), Flags));

                _assetsLoadHook = new Hook(
                    typeof(ETGMod.Assets).GetMethod(nameof(ETGMod.Assets.Load)),
                    typeof(AssetManager).GetMethod(nameof(OnAssetsLoad), Flags));

                _assetsReplaceTextureHook = new Hook(
                    typeof(ETGMod.Assets).GetMethod(nameof(ETGMod.Assets.ReplaceTexture)),
                    typeof(AssetManager).GetMethod(nameof(OnReplaceTexture), Flags));

                IsEnabled = config.Enabled;
                if (IsEnabled)
                {
                    foreach (var mod in ETGMod.AllMods)
                    {
                        if (string.IsNullOrEmpty(mod.Metadata.Archive))
                            continue;

                        if (_cachedZipFiles.ContainsKey(mod.Metadata.Archive))
                            continue;

                        using (var zip = ZipFile.Read(mod.Metadata.Archive))
                        {
                            var files = new Dictionary<string, byte[]>();
                            foreach (var entry in zip.Entries)
                            {
                                if (entry.IsDirectory)
                                    continue;

                                if (!entry.FileName.StartsWith("sprites/"))
                                    continue;

                                StatsManager.AssetManagerBytesCached += entry.UncompressedSize;
                                var data = new byte[(int)entry.UncompressedSize];
                                var memoryStream = new MemoryStream(data);
                                entry.Extract(memoryStream);
                                files[entry.FileName] = data;
                            }

                            _cachedZipFiles[mod.Metadata.Archive] = files;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                StartupLogReporter.AddError(nameof(AssetManager), $"Initialize error: {e}");
                IsEnabled = false;
            }

            StatsManager.AssetManagerStartupStopwatch.Stop();
        }

        private static Stream OnAssetMetadataGetStream(Func<AssetMetadata, Stream> orig, AssetMetadata self)
        {
            if (!Instance.IsEnabled)
            {
                return orig(self);
            }

            if (!self.HasData || self.Container != AssetMetadata.ContainerType.Zip)
            {
                return orig(self);
            }

            if (Instance._cachedZipFiles.TryGetValue(self.Zip, out var fileCache))
            {
                if (fileCache.TryGetValue(self.File, out var data))
                {
                    return new MemoryStream(data, true);
                }
            }

            return orig(self);
        }

        private static byte[] OnAssetMetadataGetData(Func<AssetMetadata, byte[]> orig, AssetMetadata self)
        {
            if (!Instance.IsEnabled)
            {
                return orig(self);
            }

            if (!self.HasData || self.Container != AssetMetadata.ContainerType.Zip)
            {
                return orig(self);
            }

            if (Instance._cachedZipFiles.TryGetValue(self.Zip, out var fileCache))
            {
                if (fileCache.TryGetValue(self.File, out var data))
                {
                    var copy = new byte[data.Length];
                    Buffer.BlockCopy(data, 0, copy, 0, data.Length);
                    return copy;
                }
            }

            return orig(self);
        }

        private static UnityObject OnAssetsLoad(Func<string, Type, UnityObject> orig, string path, Type type)
        {
            StatsManager.AssetsLoadStopwatch.Start();
            try
            {
                return orig(path, type);
            }
            finally
            {
                StatsManager.AssetsLoadStopwatch.Stop();
            }
        }

        private static void OnReplaceTexture(Action<tk2dSpriteDefinition, Texture2D, bool> orig, tk2dSpriteDefinition frame, Texture2D replacement, bool pack)
        {
            StatsManager.ReplaceTextureStopwatch.Start();
            try
            {
                orig(frame, replacement, pack);
            }
            finally
            {
                StatsManager.ReplaceTextureStopwatch.Stop();
            }
        }
    }
}
