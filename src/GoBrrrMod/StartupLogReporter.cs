﻿using System.Collections.Generic;

namespace GoBrrrMod
{
    internal static class StartupLogReporter
    {
        private readonly static List<LogMessage> DeferredMessages = new List<LogMessage>();

        public static void AddError(string source, string message)
        {
            DeferredMessages.Add(new LogMessage(source, message, LogLevel.Error));
        }

        public static void AddMessage(string source, string message)
        {
            DeferredMessages.Add(new LogMessage(source, message, LogLevel.Info));
        }

        /// <summary>
        /// Print and clear current messages. Returns the number of errors.
        /// </summary>
        /// <returns></returns>
        public static int PrintAndClear()
        {
            int count = 0;
            foreach (var message in DeferredMessages)
            {
                string stringMessage = $"{message.Source} {message.Message}";
                if (message.Level == LogLevel.Error)
                {
                    count++;
                    EtgModLogHelper.LogWarning(stringMessage);
                }
                else
                {
                    ETGModConsole.Log(stringMessage);
                }
            }

            DeferredMessages.Clear();
            return count;
        }

        private readonly struct LogMessage
        {
            public LogMessage(string source, string message, LogLevel level)
            {
                Source = source;
                Message = message;
                Level = level;
            }

            public string Source { get; }

            public string Message { get; }

            public LogLevel Level { get; }
        }

        private enum LogLevel
        {
            Info,
            Error
        }
    }
}
