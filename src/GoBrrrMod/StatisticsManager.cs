﻿using System.Diagnostics;

namespace GoBrrrMod
{
    public class StatisticsManager
    {
        private StatisticsManager()
        {
        }

        public static StatisticsManager Instance { get; } = new StatisticsManager();

        public Stopwatch PackerPackStopwatch = new Stopwatch();

        public Stopwatch PagePackStopwatch = new Stopwatch();

        public Stopwatch AtlasManagerInitalizeStopwatch = new Stopwatch();

        public Stopwatch AssetsLoadStopwatch = new Stopwatch();

        public Stopwatch ReplaceTextureStopwatch = new Stopwatch();

        public Stopwatch AssetManagerStartupStopwatch = new Stopwatch();

        public long AssetManagerBytesCached = 0;

        public int PackCallsIntercepted = 0;
    }
}
