﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GoBrrrMod
{
    public static class RuntimeAtlasPageHelper
    {
        private static FieldInfo ChangesField = typeof(RuntimeAtlasPage).GetField("_Changes", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        private static FieldInfo RectsField = typeof(RuntimeAtlasPage).GetField("_Rects", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        public static List<Rect> GetRects(this RuntimeAtlasPage page)
        {
            return (List<Rect>)RectsField.GetValue(page);
        }

        public static int IncrementChanges(this RuntimeAtlasPage page)
        {
            var changes = (int)ChangesField.GetValue(page);
            int newValue = changes + 1;
            ChangesField.SetValue(page, newValue);
            return newValue;
        }

        public static float SumTotalRectArea(this RuntimeAtlasPage page)
        {
            var rects = page.GetRects();
            float area = 0;
            foreach (var rect in rects)
            {
                area += rect.width * rect.height;
            }

            return area;
        }
    }
}
