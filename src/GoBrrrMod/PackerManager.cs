﻿using System;
using System.Reflection;
using GoBrrrMod.Configuration;
using MonoMod.RuntimeDetour;
using UnityEngine;

namespace GoBrrrMod
{
    public class PackerManager
    {
        private static StatisticsManager StatsManager = StatisticsManager.Instance;

        private Hook _packerPackHook;
        private Hook _pagePackHook;

        private PackerManager()
        {
        }

        public static PackerManager Instance { get; } = new PackerManager();

        public int MaxCachePages;

        public bool IsEnabled;

        public EnhancedAtlasManager PageManager;

        public void Initialize(PackerConfiguration config)
        {
            try
            {
                const BindingFlags Flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
                _packerPackHook = new Hook(
                    typeof(RuntimeAtlasPacker).GetMethod(nameof(RuntimeAtlasPacker.Pack)),
                    typeof(PackerManager).GetMethod(nameof(OnAtlasPackerPack), Flags));

                _pagePackHook = new Hook(
                    typeof(RuntimeAtlasPage).GetMethod(nameof(RuntimeAtlasPage.Pack)),
                    typeof(PackerManager).GetMethod(nameof(OnAtlasPagePack), Flags));

                IsEnabled = config.Enabled;
                MaxCachePages = Math.Max(config.MaxCachePages, 1);
            }
            catch (Exception e)
            {
                StartupLogReporter.AddError(nameof(PackerManager), $"Initialize error: {e}");
                IsEnabled = false;
            }
        }

        private static RuntimeAtlasSegment OnAtlasPackerPack(Func<RuntimeAtlasPacker, Texture2D, bool, RuntimeAtlasSegment> orig, RuntimeAtlasPacker self, Texture2D tex, bool apply)
        {
            try
            {
                //StatsManager.PackerPackStopwatch.Start();
                if (!Instance.IsEnabled)
                {
                    return orig(self, tex, apply);
                }

                StatsManager.PackCallsIntercepted++;

                if (Instance.PageManager == null)
                {
                    Instance.PageManager = new EnhancedAtlasManager(self)
                    {
                        MaxCacheSize = Instance.MaxCachePages
                    };

                    Instance.PageManager.Initialize();
                }

                var segment = Instance.PageManager.Pack(tex, apply);
                return segment;
            }
            finally
            {
                //StatsManager.PackerPackStopwatch.Stop();
            }
        }

        private static RuntimeAtlasSegment OnAtlasPagePack(Func<RuntimeAtlasPage, Texture2D, bool, RuntimeAtlasSegment> orig, RuntimeAtlasPage self, Texture2D tex, bool apply)
        {
            try
            {
                //StatsManager.PagePackStopwatch.Start();
                return orig(self, tex, apply);
            }
            finally
            {
                //StatsManager.PagePackStopwatch.Stop();
            }
        }
    }
}
