﻿using System.Diagnostics;

namespace GoBrrrMod
{
    internal static class StopwatchExtensions
    {
        public static string ToStringTotalSeconds(this Stopwatch stopwatch)
        {
            string seconds = stopwatch.Elapsed.TotalSeconds.ToString("0.000");
            return seconds;
        }
    }
}
