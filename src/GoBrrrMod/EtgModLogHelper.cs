﻿namespace GoBrrrMod
{
    internal static class EtgModLogHelper
    {
        // magenta
        public const string WarningColor = "ff00ffff";

        public static void LogWarning(string message)
        {
            ETGModConsole.Log($"<color=#{WarningColor}>{message}</color>");
        }
    }
}
