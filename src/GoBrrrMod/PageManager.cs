﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GoBrrrMod
{
    public class PageManager
    {
        public PageManager(RuntimeAtlasPage page)
        {
            Page = page;
            PageRects = Page.GetRects();
            TextureRect = new Rect(0, 0, page.Texture.width, page.Texture.height);
        }

        public RuntimeAtlasPage Page;

        public Rect TextureRect;

        public float StartingX;

        public float CurrentY;

        public float NextYLine;

        public List<Rect> PageRects;

        public bool TryPlaceOnPage(Texture2D inputTexture, out RuntimeAtlasSegment segment)
        {
            var texRect = new Rect();
            texRect.Set(StartingX + Page.Padding, CurrentY + Page.Padding, inputTexture.width + Page.Padding, inputTexture.height + Page.Padding);

            if (TextureRect.Contains(texRect))
            {
                StartingX = texRect.xMax;
                NextYLine = Math.Max(texRect.yMax, NextYLine);
            }
            else
            {
                StartingX = 0;
                CurrentY = NextYLine;

                texRect.Set(StartingX + Page.Padding, CurrentY + Page.Padding, inputTexture.width + Page.Padding, inputTexture.height + Page.Padding);

                if (TextureRect.Contains(texRect))
                {
                    StartingX = texRect.xMax;
                    NextYLine = Math.Max(texRect.yMax, NextYLine);
                }
                else
                {
                    segment = null;
                    return false;
                }
            }

            PageRects.Add(texRect);

            segment = new RuntimeAtlasSegment()
            {
                texture = Page.Texture,
                x = Mathf.RoundToInt(texRect.x),
                y = Mathf.RoundToInt(texRect.y),
                width = inputTexture.width,
                height = inputTexture.height
            };

            Page.Segments.Add(segment);

            Page.Texture.SetPixels32(segment.x, segment.y, segment.width, segment.height, inputTexture.GetPixels32());

            Page.IncrementChanges();

            return true;
        }

        /// <summary>
        /// Called when we'd like to recompute our internals for page that was already used by the builtin packer.
        /// </summary>
        public void RealignForExistingPage()
        {
            var rects = Page.GetRects();
            StartingX = 0; // just always start a new row.
            foreach (var rect in rects)
            {
                CurrentY = rect.yMax;
            }

            NextYLine = CurrentY;
        }
    }
}
