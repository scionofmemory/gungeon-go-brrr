﻿using System;
using System.Collections.Generic;
using System.Reflection;
using GoBrrrMod.Configuration;
using MonoMod.RuntimeDetour;
using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityComponent = UnityEngine.Component;

namespace GoBrrrMod
{
    public class ObjectHooksManager
    {
        private Hook _onHandleGameObjectHook;
        private Hook _onHandleComponentHook;
        private Hook _onHandleObjectHook;

        private ObjectHooksManager()
        {
        }

        public static ObjectHooksManager Instance { get; } = new ObjectHooksManager();

        public bool IsEnabled;

        public void Initialize(ObjectHooksConfiguration objectHooks)
        {
            try
            {
                const BindingFlags Flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
                MethodInfo from, to;

                from = typeof(ETGMod.Objects).GetMethod(nameof(ETGMod.Objects.HandleObject), BindingFlags.Public | BindingFlags.Static);
                to = typeof(ObjectHooksManager).GetMethod(nameof(OnHandleObject), Flags);
                _onHandleObjectHook = new Hook(from, to);

                from = typeof(ETGMod.Objects).GetMethod(nameof(ETGMod.Objects.HandleGameObject), BindingFlags.Public | BindingFlags.Static);
                to = typeof(ObjectHooksManager).GetMethod(nameof(OnHandleGameObject), Flags);
                _onHandleGameObjectHook = new Hook(from, to);

                from = typeof(ETGMod.Objects).GetMethod(nameof(ETGMod.Objects.HandleComponent), BindingFlags.Public | BindingFlags.Static);
                to = typeof(ObjectHooksManager).GetMethod(nameof(OnHandleComponent), Flags);
                _onHandleComponentHook = new Hook(from, to);

                IsEnabled = objectHooks.Enabled;
            }
            catch (Exception e)
            {
                StartupLogReporter.AddError(nameof(ObjectHooksManager), $"Initialize error: {e}");
                IsEnabled = false;
            }
        }

        private static void OnHandleGameObject(Action<GameObject, bool> orig, GameObject obj, bool recursive)
        {
            if (UseOriginalRecipe())
            {
                orig(obj, recursive);
            }
        }

        public static void OnHandleObject(Action<UnityObject> orig, UnityObject obj)
        {
            if (UseOriginalRecipe())
            {
                orig(obj);
            }
        }

        private static void OnHandleComponent(Action<UnityComponent> orig, UnityComponent comp)
        {
            if (UseOriginalRecipe())
            {
                orig(comp);
            }
        }

        private static bool UseOriginalRecipe()
        {
            // user has used mtg console to set these so they explicitly want 
            // sprites dumped and we will call the original logic.
            if (ETGMod.Assets.DumpSprites || ETGMod.Assets.DumpSpritesMetadata)
                return true;

            return !Instance.IsEnabled;
        }
    }
}
