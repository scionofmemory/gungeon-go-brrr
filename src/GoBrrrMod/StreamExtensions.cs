﻿using System.IO;

namespace GoBrrrMod
{
    internal static class StreamExtensions
    {
        // copied from .NET Reference Source as largest buffer size
        // before going to large object heap
        private const int DefaultCopyBufferSize = 81920;

        public static void CopyTo(this Stream input, Stream output)
        {
            CopyTo(input, output, DefaultCopyBufferSize);
        }

        public static void CopyTo(this Stream input, Stream output, int bufferSize)
        {
            byte[] buffer = new byte[bufferSize];
            int bytesRead;

            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, bytesRead);
            }
        }
    }
}
