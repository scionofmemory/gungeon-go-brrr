﻿using System;
using System.Collections.Generic;
using System.IO;
using GoBrrrMod.Configuration;
using Newtonsoft.Json;
using UnityEngine;

namespace GoBrrrMod
{
    public class GoBrrrModule : ETGModule
    {
        public static string ConfigPath = Path.Combine(Path.GetFullPath(ETGMod.ResourcesDirectory), "gungeongobrrr.json");

        public static readonly string ModName = "GungeonGoBrrr";

        public static StatisticsManager StatsManager = StatisticsManager.Instance;

        public static ModConfiguration Config;

        public static AssetManager AssetManager = AssetManager.Instance;
        public static ObjectHooksManager ObjectHooksManager = ObjectHooksManager.Instance;
        public static PackerManager PackerManager = PackerManager.Instance;

        public override void Init()
        {
            Config = ReadConfiguration();
            PackerManager.Initialize(Config.Packer);
            AssetManager.Initialize(Config.Assets);
            ObjectHooksManager.Initialize(Config.ObjectHooks);
        }

        public override void Start()
        {
            try
            {
                ETGModConsole.Commands.AddGroup("ggb", args =>
                {
                    if (args.Length == 0)
                    {
                        ETGModConsole.Log($"PackerManager.Enabled: {PackerManager.IsEnabled}");
                        ETGModConsole.Log($"AssetManager.Enabled: {AssetManager.IsEnabled}");
                        ETGModConsole.Log($"ObjectHooksManager.Enabled: {ObjectHooksManager.IsEnabled}");
                    }
                    else
                    {
                        ETGModConsole.Log("Unknown command arguments");
                        ETGModConsole.Log("Valid commands: details, writeconfig");
                    }
                });

                ETGModConsole.Commands.GetGroup("ggb").AddUnit("details", args =>
                {
                    ETGModConsole.Log($"PackerManager.Enabled: {PackerManager.IsEnabled}");
                    if (PackerManager.IsEnabled)
                    {
                        ETGModConsole.Log($"Managed pages: {PackerManager.PageManager?.AllManagedPages.Count}");
                    }
                    ETGModConsole.Log($"MaxCachePages: {PackerManager.MaxCachePages}");
                    ETGModConsole.Log($"Allocated pages: {ETGMod.Assets.Packer.Pages.Count}");
                    ETGModConsole.Log($"Atlas page size: {ETGMod.Assets.Packer.Width} x {ETGMod.Assets.Packer.Height}");
                    ETGModConsole.Log($"Total elapsed in Packer.Pack: {StatsManager.PackerPackStopwatch.ToStringTotalSeconds()}s");
                    ETGModConsole.Log($"Packer.Pack calls intercepted: {StatsManager.PackCallsIntercepted}");
                    ETGModConsole.Log($"Total elapsed in Page.Pack: {StatsManager.PagePackStopwatch.ToStringTotalSeconds()}s");

                    ETGModConsole.Log($"AssetHooks.Enabled: {AssetManager.IsEnabled}");
                    ETGModConsole.Log($"AssetManager.Initialize elapsed: {StatsManager.AssetManagerStartupStopwatch.ToStringTotalSeconds()}s");
                    ETGModConsole.Log($"AssetManager cached bytes: {StatsManager.AssetManagerBytesCached} bytes");
                    ETGModConsole.Log($"Total elapsed in Asset Load: {StatsManager.AssetsLoadStopwatch.ToStringTotalSeconds()}s");
                    ETGModConsole.Log($"Total elapsed in ReplaceText: {StatsManager.ReplaceTextureStopwatch.ToStringTotalSeconds()}s");

                    ETGModConsole.Log($"ObjectHooks.Enabled: {ObjectHooksManager.IsEnabled}");
                    ETGModConsole.Log($"DumpSprites: {ETGMod.Assets.DumpSprites}");
                    ETGModConsole.Log($"DumpSpritesMetadata: {ETGMod.Assets.DumpSpritesMetadata}");
                });

                ETGModConsole.Commands.GetGroup("ggb").AddUnit("writeconfig", args =>
                {
                    WriteConfiguration(Config);
                });

#if DEBUG
                ETGModConsole.Commands.GetGroup("ggb").AddUnit("debug", args =>
                {
                    ETGModConsole.Log($"Total Atlas pages: {ETGMod.Assets.Packer.Pages.Count}");
                    int index = 0;
                    foreach (var page in ETGMod.Assets.Packer.Pages)
                    {

                        float covered = page.SumTotalRectArea();
                        long total = page.Texture.width * page.Texture.height;
                        float coverage = covered / total * 100;
                        ETGModConsole.Log($"Page[{index}] coverage: {coverage:0.00}");
                        index++;
                    }
                });
#endif

                int errors = StartupLogReporter.PrintAndClear();

                if (errors > 0)
                {
                    EtgModLogHelper.LogWarning($"{ModName} v{GoBrrrModAssembly.Version} setup failed");
                }
                else
                {
                    ETGModConsole.Log($"Loaded {ModName} v{GoBrrrModAssembly.Version} successfully");
                }
            }
            catch (Exception e)
            {
                EtgModLogHelper.LogWarning($"Failed to load {ModName} v{GoBrrrModAssembly.Version}");
                ETGModConsole.Log($"Exception: {e}");
            }
        }

        public override void Exit()
        {
        }

        private static ModConfiguration ReadConfiguration()
        {
            try
            {
                if (File.Exists(ConfigPath))
                {
                    StartupLogReporter.AddMessage("ReadConfiguration", $"GoBrrrMod reading config from {ConfigPath}");
                    string json = File.ReadAllText(ConfigPath);
                    var config = JsonConvert.DeserializeObject<ModConfiguration>(json);

                    if (config.Packer == null)
                        config.Packer = new PackerConfiguration();

                    if (config.ObjectHooks == null)
                        config.ObjectHooks = new ObjectHooksConfiguration();

                    if (config.Assets == null)
                        config.Assets = new AssetConfiguration();

                    return config;
                }
                else
                {
                    return new ModConfiguration();
                }
            }
            catch (Exception e)
            {
                StartupLogReporter.AddError("ReadConfiguration", $"Error reading configuration: {e}");
                return new ModConfiguration();
            }
        }

        private static void WriteConfiguration(ModConfiguration config)
        {
            try
            {
                string json = JsonConvert.SerializeObject(config, Formatting.Indented);
                File.WriteAllText(ConfigPath, json);
                ETGModConsole.Log($"Wrote configuration to {ConfigPath}");
            }
            catch (Exception e)
            {
                ETGModConsole.Log($"Error writing configuration configuration to {ConfigPath}: {e}");
            }
        }
    }
}
