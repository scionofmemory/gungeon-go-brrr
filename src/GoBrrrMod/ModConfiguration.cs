﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GoBrrrMod.Configuration
{
    public class ModConfiguration
    {
        public PackerConfiguration Packer = new PackerConfiguration();

        public ObjectHooksConfiguration ObjectHooks = new ObjectHooksConfiguration();

        public AssetConfiguration Assets = new AssetConfiguration();
    }

    public class PackerConfiguration
    {
        public bool Enabled = true;

        public int MaxCachePages = 1;
    }

    public class ObjectHooksConfiguration
    {
        public bool Enabled = false;
    }

    public class AssetConfiguration
    {
        public bool Enabled = true;
    }
}
